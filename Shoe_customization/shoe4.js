﻿
var game = new Phaser.Game(800, 600, Phaser.CANVAS, 'phaser-example', { preload: preload, create: create, update: update });

function preload() {

    game.load.image('shoe', 'shoe_designs/shoe4.png');
    game.load.image('wave', 'shoe_designs/top4.png');
    game.load.image('bottom', 'shoe_designs/middle4.png');
    game.load.image('brick_wall', 'brick_wall.jpg');

    game.load.image('red', 'white.png');
    game.load.image('blue', 'white.png');
    game.load.image('yellow', 'white.png');
    game.load.image('white', 'white.png');
    game.load.image('green', 'white.png');
    game.load.image('orange', 'white.png');
    game.load.image('violet', 'white.png');

}

var sprite;
var wave;
var front;
var bottom;
var selected;

var red;
var blue;
var yellow;
var white;
var green;
var orange;
var violet;

var color;
var selected_color;
var shoe3;

function create() {


    game.add.image(0, 0, 'brick_wall');

    red = game.add.button(50, 50, 'red', changeRed, this);
    red.tint = 0xed1c24;

    blue = game.add.button(250, 50, 'blue', changeBlue, this);
    blue.tint = 0x0054a6;

    yellow = game.add.button(450, 50, 'yellow', changeYellow, this);
    yellow.tint = 0xfff200;

    white = game.add.button(650, 50, 'white', changeWhite, this);
    white.tint = 0xffffff;

    green = game.add.button(50, 200, 'green', changeGreen, this);
    green.tint = 0x00a651;

    orange = game.add.button(50, 350, 'orange', changeOrange, this);
    orange.tint = 0xf7941d;

    violet = game.add.button(50, 500, 'violet', changeViolet, this);
    violet.tint = 0x662d91;


    shoe3 = game.add.group();

    sprite = game.add.button(game.world.centerX, game.world.centerY, 'shoe', changeShoeColor, this);
    sprite.anchor.set(0.5);

    shoe3.add(sprite);

    sprite.tint = 0xf26522;//Math.random() * 0xffffff;



    wave = game.add.button(sprite.x - 205, sprite.y - 105, 'wave', changeWaveColor, this);
    //top = game.add.button(sprite.x -224, sprite.y -145, 'top', changeTopColor, this);
    shoe3.add(wave);


    bottom = game.add.button(sprite.x -130, sprite.y -165, 'bottom', changeBottomColor, this);

    shoe3.add(bottom);

    sprite.events.onInputDown.add(selector, this);
    wave.events.onInputDown.add(selector, this);
    front.events.onInputDown.add(selector, this);
    bottom.events.onInputDown.add(selector, this);



}


function changeRed() {

    color = 0xed1c24;

}

function changeBlue() {

    color = 0x0054a6;

    console.log(color);
}

function changeYellow() {
    color = 0xfff200;

    console.log("this is yellow:" + color);
}

function changeWhite() {
    color = 0xffffff;

    //   console.log("this is yellow:" + color);
}

function changeGreen() {
    color = 0x00a651;

    //   console.log("this is yellow:" + color);
}

function changeOrange() {
    color = 0xf7941d;

    //   console.log("this is yellow:" + color);
}

function changeViolet() {
    color = 0x662d91;

    //   console.log("this is yellow:" + color);
}

function changeShoeColor() {

    sprite.tint = color;

}

function changeWaveColor() {

    wave.tint = color;


}



function changeBottomColor() {

    bottom.tint = color;
}


function update() {

    //console.log("This is the selected object:"+selected);


}
